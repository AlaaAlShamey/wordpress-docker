FROM wordpress:latest

RUN docker-php-ext-install pdo pdo_mysql calendar

COPY ./ssl /etc/apache2/ssl
COPY ./site-conf/wordpress.conf /etc/apache2/sites-available/wordpress.conf

RUN a2enmod ssl && a2ensite wordpress.conf && service apache2 restart